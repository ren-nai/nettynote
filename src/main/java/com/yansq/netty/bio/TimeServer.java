package com.yansq.netty.bio;

import io.netty.util.concurrent.DefaultThreadFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yansq
 * @date 2018/9/16 16:04
 */
public class TimeServer {

     public static void main(String[] args) throws IOException {
         int port = 8080;
         if (args!= null && args.length>0){
             try {
                 port = Integer.valueOf(args[0]);
             } catch (NumberFormatException e){
                 // 使用默认端口
             }
         }
         ServerSocket server = null;
         try {
             server = new ServerSocket(port);
             System.out.println("the time server is start in port : " + port);
             Socket socket;
             while(true){
                 socket = server.accept();
//                 new Thread(new TimeServerHandler(socket)).start();
                 new ThreadPoolExecutor(50,100,0L,TimeUnit.MILLISECONDS,
                         new ArrayBlockingQueue<Runnable>(1000),
                         Executors.defaultThreadFactory());
             }
         } finally {
            if(server !=null){
                System.out.println("the time server is closing ");
                server.close();
                server = null;
            }
         }
     }
}
