package com.yansq.netty.bio;

import java.io.*;
import java.net.Socket;

/**
 * @author yansq
 * @date 2018/9/16 16:47
 */
public class TimeClient {
    public static void main(String[] args){
        int port = 8080;
        if(args != null && args.length>0){
            try {
                port = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                // 使用8080
            }
        }
        Socket socket = null;
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            socket = new Socket("127.0.0.1",port);
            in =  new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(),true);
            out.println("1");
            System.out.println("send order 2 server succeed");
            String resp = in.readLine();
            System.out.println("Now is : " + resp );
        } catch (IOException e) {

        } finally {
            if(out != null){
                out.close();
                out = null;
            }
            if (in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if(socket != null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                socket = null;
            }
        }
    }
}
