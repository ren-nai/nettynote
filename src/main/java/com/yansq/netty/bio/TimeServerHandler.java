package com.yansq.netty.bio;

import java.io.*;
import java.net.Socket;
import java.util.Date;

/**
 * @author yansq
 * @date 2018/9/16 16:12
 */
public class TimeServerHandler implements Runnable{
    private Socket socket;

    public TimeServerHandler(Socket socket){
        this.socket = socket;
    }

    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(this.socket.getOutputStream(),true);
            String currentTime = null;
            String body = null;
            while(true){
                body = in.readLine();
                if(body == null){
                    break;
                }
                System.out.println("the time server receive order : " + body);
                currentTime = "1".equalsIgnoreCase(body)? new Date().toString():"bad order";
                out.println(currentTime);
                System.out.println(currentTime);
            }

        } catch (Exception e) {
            if(in != null){
                try {
                    in.close();
                }catch (IOException e1){
                    e1.printStackTrace();
                }
                if(out!= null){
                    out.close();
                    out = null;
                }
                if(this.socket != null){
                    try {
                        this.socket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    this.socket = null;
                }
            }
        }
    }
}
