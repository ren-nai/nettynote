package com.yansq.netty.nio;

/**
 * @author yansq
 * @date 2018/9/16 21:09
 */
public class TimeServer {
    public static void main(String[] args){
        int port = 8080;
        MutiplexerTimeServer timeServer = new MutiplexerTimeServer(port);
        new Thread(timeServer,"nio-mutiplexertimeServer-001").start();
    }
}
